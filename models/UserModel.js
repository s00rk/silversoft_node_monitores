module.exports = function(sequelize){

	var Sequelize = require('sequelize');

	var User = sequelize.define('User',
		{
			id: { type:Sequelize.INTEGER, primaryKey: true },
			username: Sequelize.STRING,
			email: Sequelize.STRING
		},
		{
			timestamps: false,
			freezeTableName: true,
			tableName: 'auth_user'
		}
	);

	return User;

};