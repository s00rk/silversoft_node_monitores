module.exports = function(sequelize){

	var Sequelize = require('sequelize');

	var SignoVital = sequelize.define('SignoVital',
		{
			res_id: Sequelize.INTEGER,
			temperatura: Sequelize.DECIMAL(6, 2),
			frecuencia_cardiaca: Sequelize.DECIMAL(6, 2),
			frecuencia_respiratoria: Sequelize.DECIMAL(6, 2),
			fecha: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
		},
		{
			timestamps: false,
			freezeTableName: true,
			tableName: 'res_signo_vital'
		}
	);

	return SignoVital;

};