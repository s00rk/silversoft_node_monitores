module.exports = function(sequelize){

	var Sequelize = require('sequelize');

	var Alerta = sequelize.define('Alerta',
		{
			res_id: Sequelize.INTEGER,
			estado: { type: Sequelize.STRING, defaultValue: 'checar' },
			fecha: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
			updatedAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
			comentario: { type: Sequelize.STRING, defaultValue: '' }
		},
		{
			timestamps: false,
			freezeTableName: true,
			tableName: 'res_alerta'
		}
	);

	return Alerta;

};