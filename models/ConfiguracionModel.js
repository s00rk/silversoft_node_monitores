module.exports = function(sequelize){

	var Sequelize = require('sequelize');

	var Configuracion = sequelize.define('Configuracion',
		{
			temperatura: Sequelize.STRING,
			frespiratoria: Sequelize.STRING,
			fcardiaca: Sequelize.STRING,
			tiempo_peticion_res: Sequelize.INTEGER,
			tiempo_peticion_res_enferma: Sequelize.INTEGER,
			intentos_email: Sequelize.INTEGER,
			tiempo_email: Sequelize.INTEGER
		},
		{
			timestamps: false,
			freezeTableName: true,
			tableName: 'planta_configuracion'
		}
	);

	return Configuracion;

};