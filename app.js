var express 		= require('express'),
	app				= express(),
	server			= require('http').Server(app),
	io				= require('socket.io')(server),
	mailer 			= require('express-mailer'),	
	cookieParser 	= require('cookie-parser'),
	bodyParser 		= require('body-parser'),
	Sequelize		= require('sequelize');

var connectPG = 'postgres://silversoft_db_user:silversoft_db_password@web506.webfaction.com/silversoft_monitores';
var sequelize = new Sequelize(connectPG, { logging: false });

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

mailer.extend(app, {
	from: 'victor@dreamsoft.st',
	host: 'smtp.webfaction.com', 
	secureConnection: true,
	port: 465,
	transportMethod: 'SMTP',
	auth: {
		user: 'dreamsoft',
		pass: 'saske321.'
	}
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());

app.use('/', require('./controllers/SignoVitalController')(app, io, sequelize) );

server.listen(23493, function(){
	console.log('Server Initialized');
});