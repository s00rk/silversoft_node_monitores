module.exports = function(app, io, sequelize)
{
	var express 	= require( "express" ),
	router 			= express.Router(),
	pg 				= require('pg'),
	Signo_Vital 	= require('../models/Signo_VitalModel')(sequelize),
	Alerta 			= require('../models/AlertaModel')(sequelize),
	Configuracion 	= require('../models/ConfiguracionModel')(sequelize),
	User 			= require('../models/UserModel')(sequelize);


	var ReceiveSignoVital = function(req, res){
		var temperatura = req.body.temperatura;
		var frecuencia_cardiaca = req.body.frecuencia_cardiaca;
		var frecuencia_respiratoria = req.body.frecuencia_respiratoria;
		var res_id = req.body.res;

		if(!res_id || !temperatura || !frecuencia_cardiaca || !frecuencia_respiratoria)
		{
			res.send('NO DATA');
			return;
		}

		data = { 
			'temperatura': temperatura,
			'frecuencia_respiratoria': frecuencia_respiratoria,
			'frecuencia_cardiaca': frecuencia_cardiaca,
			'res_id': res_id
		};
		data_send = { 
			'temperatura': temperatura,
			'frecuencia_respiratoria': frecuencia_respiratoria,
			'frecuencia_cardiaca': frecuencia_cardiaca,
			'res': res_id
		};
		io.emit('signo_vital', data_send);
		Signo_Vital.create(data);
		
		Configuracion.findOne().then(function (conf){
			var temp = JSON.parse( conf.temperatura );
			var temp_max = temp.moderada['max'];
			var temp_min = temp.moderada['min'];

			var fres = JSON.parse( conf.frespiratoria );
			var fres_max = temp.moderada['max'];
			var fres_min = temp.moderada['min'];

			var fcard = JSON.parse( conf.fcardiaca );
			var fcard_max = temp.moderada['max'];
			var fcard_min = temp.moderada['min'];
			var tiempo_email = conf.tiempo_email;

			var msj = ('La res #' + res_id + ' debe ser atendida. <p>Temperatura actual es de ' + temperatura + 'C</p><p>Frecuencia Respiratoria: ' + frecuencia_respiratoria + 'RPM</p><p>Frecuencia Cardiaca: ' + frecuencia_cardiaca + 'LPM');
			Alerta.findAndCountAll({ where: {'res_id': res_id}}).then(function(results) {
				if(results.count == 0)
				{
					if(frecuencia_cardiaca > fcard_max || frecuencia_cardiaca < fcard_min || frecuencia_respiratoria < fres_min || frecuencia_respiratoria > fres_max || temperatura > temp_max || temperatura < temp_min)
					{	
						var hoy = new Date();
						hoy.setMinutes(hoy.getMinutes()+parseInt(tiempo_email));
						Alerta.create({'res_id': res_id, 'updatedAt': hoy});
						User.findAll({}).then(function(users){
							correos = '';
							users.forEach(function(user){
								correos = correos + user.email + ',';
							});

							if(correos.length > 0)
							{
								correos = correos.slice(0, -1);
								app.mailer.send('alert_email', {
									to: correos, 
									subject: 'Alerta Res ~ MonitoRes',
									message: msj
								}, function (err) {
									if (err) {
										console.log(err);
										return;
									}
								});	
							}
						});
						io.emit('alerta', {
							'res': res_id,
							'temperatura': temperatura,
							'estado': 'checar'
						});
						console.log('enviar');
					}
					
				}else{
					Alerta.findOne({ where: {'res_id': res_id}, order: [['id', 'DESC']] }).then(function (alerta){
						var fecha = new Date();
						var hoy = new Date();
						var enviar = false;
						estado = alerta.estado;
						fecha = new Date(alerta.updatedAt.getTime());
						

						if(frecuencia_cardiaca > fcard_max || frecuencia_cardiaca < fcard_min || frecuencia_respiratoria < fres_min || frecuencia_respiratoria > fres_max || temperatura > temp_max || temperatura < temp_min)
						{
							if(estado.localeCompare('checar') == 0 && hoy.getTime() > fecha.getTime()){
								enviar = true;
							}else if(estado.localeCompare('finalizado') == 0){
								enviar = true;
								var hoy = new Date();
								hoy.setMinutes(hoy.getMinutes()+parseInt(tiempo_email));
								Alerta.create({'res_id': res_id, 'updatedAt': hoy});
							}else if(estado.localeCompare('revisado') == 0 && hoy.getTime() > fecha.getTime()){
								enviar = true;
							}	
						}

						if(enviar)
						{
							console.log('ENVIAR');
							if(hoy.getTime() >= fecha.getTime()){
								if(estado.localeCompare('finalizado') != 0)
								{
									hoy.setMinutes(hoy.getMinutes()+parseInt(tiempo_email));
									alerta.updatedAt = hoy;
									alerta.save();
								}
							}
							User.findAll({}).then(function(users){
								correos = '';
								users.forEach(function(user){
									correos = correos + user.email + ',';
								});
								console.log(correos);
								if(correos.length > 0)
								{
									correos = correos.slice(0, -1);
									app.mailer.send('alert_email', {
										to: correos, 
										subject: 'Alerta Res ~ MonitoRes',
										message: msj
									}, function (err) {
										if (err) {
											console.log(err);
											return;
										}
									});	
								}
							});
											

							Alerta.findOne({ where: {'res_id': res_id}, order: [['id', 'DESC']] }).then(function (alertaa){
								io.emit('alerta', {
									'res': res_id,
									'temperatura': temperatura,
									'estado': alertaa.estado
								});
							});				

						}
					});
				}
			});
		});

		res.send(data);
	}

	router.route('/signo_vital/').post( ReceiveSignoVital );

	return router;
};